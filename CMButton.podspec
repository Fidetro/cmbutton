Pod::Spec.new do |s|

  s.name         = "CMButton"
  s.version      = "0.0.4"
  s.summary      = "CMButton"
  s.homepage     = "https://bitbucket.org/Fidetro/cmbutton"
  s.license          = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "fidetro" => "zykzzzz@hotmail.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://bitbucket.org/Fidetro/cmbutton.git", :tag => "0.0.4" }
  s.source_files  = "Source", "CMButton/Source/*.{h,m,swift}"

s.dependency "SnapKit"
end
