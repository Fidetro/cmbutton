//
//  CMButton.swift
//  CMButton
//
//  Created by Fidetro on 2018/10/3.
//  Copyright © 2018 karim. All rights reserved.
//

import UIKit
import SnapKit
open class CMButton: UIControl {
    public enum ImageViewDirection {
        case top
        case left
        case right
        case bottom
    }
    
    open lazy var imageView : UIImageView = {
        var imageView = UIImageView()
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    open lazy var backgroundImageView : UIImageView = {
        var backgroundImageView = UIImageView()
        backgroundImageView.isUserInteractionEnabled = true
        addSubview(backgroundImageView)
        sendSubviewToBack(backgroundImageView)
        backgroundImageView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        return backgroundImageView
    }()
    
    open lazy var titleLabel: UILabel = {
        var titleLabel = UILabel()
        return titleLabel
    }()
    
    public let direction : ImageViewDirection
    private var space_ : CGFloat = 0
    open var space : CGFloat {
        get{
            return space_
        }
        set(value){
            space_ = value
            updateSpaceLayout()
        }
    }
    public init(direction : ImageViewDirection) {
        self.direction = direction
        super.init(frame: .zero)
        snpLayoutSubview()
    }
    
    public override init(frame: CGRect) {
        self.direction = .left
        super.init(frame: .zero)
        snpLayoutSubview()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func snpLayoutSubview() {
        addSubview(titleLabel)
        addSubview(imageView)
        
        switch direction {
        case .top:
            imageView.snp.makeConstraints{
                $0.top.left.right.equalToSuperview()
            }
            titleLabel.snp.makeConstraints{
                $0.left.right.bottom.centerX.equalToSuperview()
                $0.top.equalTo(imageView.snp.bottom).offset(space)
            }
        case .bottom:
            imageView.snp.makeConstraints{
                $0.bottom.left.right.equalToSuperview()
            }
            titleLabel.snp.makeConstraints{
                $0.left.right.top.centerX.equalToSuperview()
                $0.bottom.equalTo(imageView.snp.top).offset(-space)
            }
        case .left:
            imageView.snp.makeConstraints{
                $0.left.top.bottom.centerY.equalToSuperview()
            }
            titleLabel.snp.makeConstraints{
                $0.top.bottom.right.equalToSuperview()
                $0.left.equalTo(imageView.snp.right).offset(space)
            }
        case .right:
            imageView.snp.makeConstraints{
                $0.top.bottom.right.centerY.equalToSuperview()
            }
            titleLabel.snp.makeConstraints{
                $0.top.bottom.left.equalToSuperview()
                $0.right.equalTo(imageView.snp.left).offset(-space)
            }
        }
    }
    
    public func updateSpaceLayout() {
        switch direction {
        case .top:
            titleLabel.snp.updateConstraints{
                $0.top.equalTo(imageView.snp.bottom).offset(space)
            }
        case .bottom:
            titleLabel.snp.updateConstraints{
                $0.bottom.equalTo(imageView.snp.top).offset(-space)
            }
        case .left:
            titleLabel.snp.updateConstraints{
                $0.left.equalTo(imageView.snp.right).offset(space)
            }
        case .right:
            titleLabel.snp.updateConstraints{
                $0.right.equalTo(imageView.snp.left).offset(-space)
            }
        }
    }
    
}
